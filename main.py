from bs4 import BeautifulSoup as soup
from urllib.request import urlopen as uReg

my_url = 'https://www.flipkart.com/search?q=iphone&otracker=search&otracker1=search&marketplace=FLIPKART&as-show=on&as=off'

uClient = uReg(my_url)
page_html = uClient.read()
uClient.close()
page_soup = soup(page_html, 'html.parser')

containers = page_soup.findAll('div', {'class': '_3pLy-c row'})

container = containers[0]

filename = 'products.csv'
f = open(filename, 'w')

headers = 'Product_Name, Pricing, Ratings\n'
f.write(headers)

for container in containers:
    product_container = container.findAll('div', {'class': '_4rR01T'})
    product_name = product_container[0].text

    price_container = container.findAll('div', {'class': '_30jeq3 _1_WHN1'})
    price = price_container[0].text

    rating_container = container.findAll('div', {'class': '_3LWZlK'})
    rating = rating_container[0].text

    #trimming price
    trim_price = ''.join(price.split(','))
    final_price = trim_price.strip('₹')
    
    f.write(product_name.replace(',', '|') + ',' + final_price + ',' + rating + '\n')

f.close()
